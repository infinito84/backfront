window.backfront = (template, model, methods) => {
  console.time('Backfront loading')
  let head = document.querySelector('head')
  let events = ['click']
  let ignoreAttrs = ['if', 'for', 'with']
  let socket
  let hash = {}
  let log = []

  function buildTemplate(obj, params){
    obj.fn = Function.apply(null, params.concat('$event', obj.template))
    delete obj.template
  }

  function addEvent(fn, args, event){
    let changeEvents = ['change', 'reset', 'paste', 'drop', 'input']
    if(~changeEvents.indexOf(event)) return function($event){
      fn.apply(model, args.concat($event))
      doRender(template, [])
    }
    return function($event){
      let temp1 = log.length
      fn.apply(model, args.concat($event))
      if(temp1 !== log.length) doRender(template, [])
    }
  }

  function copyAttrs(attrs){
    let newAttrs = {}
    for(let a in attrs){
      let property = {}
      let value = attrs[a]
      for(let b in value){
        if(b === 'dom') continue
        property[b] = value[b]
      }
      newAttrs[a] = property
    }
    return newAttrs
  }

  function checkAttributes(el, elem, params, args){
    if(el.bind){
      if(typeof el.bind === 'string'){
        el.bind = Function.apply(null, params.concat('v', `${el.bind} = v`))
      }
      if(el.listener){
        elem.removeEventListener('input', el.listener)
      }
      el.listener = function(){
        el.bind.apply(model, args.concat(elem.value || elem.innerText))
        doRender(template, [])
      }
      elem.addEventListener('input', el.listener)
    }
    for(let attr in el.attrs){
      let value = el.attrs[attr]
      if(value.template) buildTemplate(value, params)
      if(~ignoreAttrs.indexOf(attr)) continue
      if(~events.indexOf(attr) && value.fn){
        if(value.listener){
          elem.removeEventListener(attr, value.listener)
        }
        value.listener = addEvent(value.fn, args, attr)
        elem.addEventListener(attr, value.listener)
        continue
      }
      if(attr === 'value') continue
      let node = elem.getAttributeNode(attr)
      if(!node){
        node = document.createAttribute(attr)
        if(value.text) node.nodeValue = value.text
        elem.setAttributeNode(node)
      }
      value.dom = node
    }
  }

  function createNode(el, params, args){
    let elem
    if(el.tag){
      elem = document.createElement(el.tag)
      checkAttributes(el, elem, params, args)
    }
    else{
      elem = document.createTextNode(el.text ? el.text : '')
    }
    return elem
  }

  function assignDOMFor(el, params, args, j){
    let array = []
    if(!el.dom){
      el.dom = document.createComment(0)
      el.parent.appendChild(el.dom)
    }
    else{
      array = hash[el.dom.data] || []
    }
    buildTemplate(el.attrs.for, params)
    params.push(el.attrs.for.it, el.attrs.for.index)
    if(el.attrs.if) buildTemplate(el.attrs.if, params)
    
    el.n = array.length
    el.childrenTpl = JSON.stringify(el.children)
    el.children = []
    el.allAttrs = []
    el.allListeners = []
    el.elems = []
    el.elems2 = []
    el.params = params
    let current = el.dom
    array.forEach((item, i) => {
      if(i >= el.n) return
      let args2 = args.concat(item, i)
      if(el.tag !== 'virtual') current = current.nextSibling || createNode(el, params, args2)
      else current = current.nextSibling || document.createComment(0)
      el.elems.push(current)
      let children = JSON.parse(el.childrenTpl)
      el.children.push(children)
      j++
      if(el.tag !== 'virtual'){
        checkAttributes(el, current, params, args2, true)
        el.allAttrs.push(copyAttrs(el.attrs))
        el.allListeners.push(el.listener)
        assignDOM(current, children, params.slice(0), args2.slice(0))
      }
      else{
        j++
        el.if = current.data == 1
        if(el.children && el.if){
          assignDOM(el.parent, children, params.slice(0), args2.slice(0), j)
          j += children.length
          current = el.parent.childNodes[j]
          el.elems2.push(current)
        }
        else{
          assignDOM(el.parent, children, params.slice(0), args2.slice(0), j, true) 
          el.elems2.push(document.createComment(0))
        }
      }
    })
    return j + 1
  }
  function assignDOM(root, children, params, args, j, virtual){
    let i = j || 0
    children.forEach(function(el){
      el.parent = root
      el.dom = root.childNodes[i]
      if(el.with){
        buildTemplate(el.attrs.with, params)
        params = params.concat(el.attrs.with.var)
        args = args.concat(el.attrs.with.fn.apply(model, args))
      }
      if(el.for) return i = assignDOMFor(el, params.slice(0), args.slice(0), i)
      if(el.tag !== 'virtual'){
        if(!el.dom || virtual){
          el.dom = createNode(el, params, args)
          if(el.attrs && el.attrs.if){
            el.if = true
            el.dom2 = document.createComment(el.tag)
          }
          if(!virtual) root.appendChild(el.dom)
        }
        else{
          if(el.attrs && el.attrs.if){
            el.dom2 = el.dom.nodeType === 8 ? createNode(el, params) : document.createComment(el.tag)
            el.if = el.dom.nodeType === 1
          }
          if(el.dom.nodeType === 1) checkAttributes(el, el.dom, params, args)
        }
        if(el.template){
          buildTemplate(el, params)
        }
        if(el.children) assignDOM(el.if !== undefined && !el.if ? el.dom2 : el.dom, el.children, params.slice(0), args.slice(0))
        i++
      }
      if(el.tag === 'virtual'){
        i++
        el.if = el.dom.data == 1
        if(el.attrs.if){
          buildTemplate(el.attrs.if, params)
        }
        el.params = params.slice(0)
        if(el.children && el.if){
          i = assignDOM(root, el.children, el.params, args.slice(0), i) 
        }
        else{
          assignDOM(root, el.children, el.params, args.slice(0), i, true) 
        }
        el.dom2 = root.childNodes[i]
        i++
      }
    })
    return i
  }

  function createIterator(el, params){
    let children = JSON.parse(el.childrenTpl)
    el.children.push(children)
    let current
    if(el.tag !== 'virtual'){
      current = document.createElement(el.tag)
      el.elems.push(current)
      el.attrs = copyAttrs(el.attrs)
      checkAttributes(el, current, el.params, params)
      el.allAttrs.push(el.attrs)
      assignDOM(current, children, el.params.slice(0), params.slice(0))
    }
    else{
      current = document.createComment(1)
      el.elems.push(current)
      assignDOM(el.parent, children, el.params.slice(0), params.slice(0), Date.now(), true) 
      el.elems2.push(document.createComment(1))

    }
    return current
  }

  function renderFor(el, params){
    let array = []
    try{
      array = el.attrs.for.fn.apply(model, params) || array
    }catch(e){}
    if(array.length && el.attrs.if){
      array = array.filter((e) => el.attrs.if.fn.apply(model, params.concat(e)))
    }
    if(el.n !== array.length){
      if(el.n > array.length){
        for(let i=array.length; i<el.n; i++){
          el.parent.removeChild(el.elems[i])
          if(el.tag === 'virtual'){
            el.parent.removeChild(el.elems2[i])
            el.children[i].forEach(c => el.parent.removeChild(c.dom))
          }
        }
      }
      else{
        let current = el.n === 0 ? el.dom : el.tag !== 'virtual' ? el.elems[el.n-1] : el.elems2[el.n-1]
        for(let i=el.n; i<array.length; i++){
          let insert = el.elems[i] || createIterator(el, params.concat(array[i], i))
          el.parent.insertBefore(insert, current.nextSibling)
          current = insert
          if(el.tag === 'virtual'){
            el.parent.insertBefore(el.elems2[i], current.nextSibling)
            current = el.elems2[i]
            el.children[i].forEach(c => el.parent.insertBefore(c.dom, current))
          }
        }
      }
      el.n = array.length
    }
    array.forEach((item,i) => {
      let params2 = params.concat(item, i)
      if(el.tag !== 'virtual'){
        renderAttrs(el.allAttrs[i], params2, el.elems[i])
        render(el.children[i], params2.slice(0))
        el.attrs = el.allAttrs[i]
        el.listener = el.allListeners[i]
        checkAttributes(el, el.elems[i], el.params, params2)
        el.allListeners[i] = el.listener
      }
      else{
        render(el.children[i], params2.slice(0))
      }
    })
  }

  function renderAttrs(attrs, params, elem){
    for(let attr in attrs){
      if(attr === 'value'){
        let temp1 = elem.value
        let temp2 = attrs[attr].text
        if(attrs[attr].fn) temp2 = attrs[attr].fn.apply(model, params)
        if(temp1 !== temp2){
          elem.value = temp2
        }
      }
      attr = attrs[attr]
      if(attr.dom && attr.fn){
        let temp1 = attr.dom.nodeValue
        let temp2 = attr.fn.apply(model, params)
        if(temp1 !== temp2) attr.dom.nodeValue = temp2  
      }
    }
  }

  function render(children, params){
    children.forEach((el) => {
      if(el.with) params = params.concat(el.attrs.with.fn.apply(model, params))
      if(el.tag){
        if(el.for) return renderFor(el, params.slice(0))
        if(el.tag !== 'virtual'){
          if(el.if !== undefined){
            let newIf = !!el.attrs.if.fn.apply(model, params)
            if(newIf !== el.if){
              el.parent.insertBefore(el.dom2, el.dom)
              let temp = el.dom2
              el.dom2 = el.parent.removeChild(el.dom)
              el.dom = temp
              el.if = newIf
            }
            if(!newIf) return
          }
          renderAttrs(el.attrs, params, el.dom)
          checkAttributes(el, el.dom, false, params)
          render(el.children, params.slice(0))
          if(el.tag === 'select') renderAttrs({value: el.attrs.value} , params, el.dom)
        }
        else{
          if(el.attrs.if){
            let newIf = !!el.attrs.if.fn.apply(model, params)
            if(newIf !== el.if){
              el.if = newIf
              if(newIf){
                el.children.forEach(i => {
                  if(i.dom) i.parent.insertBefore(i.dom, el.dom2)
                })
              }
              else{
                el.children.forEach(i => i.parent.removeChild(i.dom))
              }
            }
          }
          if(el.if) render(el.children, params.slice(0))
        }
      }
      else{
        if(el.fn){
          let temp1 = el.dom.data
          let temp2 = el.fn.apply(model, params)
          if(temp1 !== temp2) el.dom.data = temp2
        }
      }
    })
  }

  function socketIO(){
    socket = io.connect(`${location.origin}?id=${model.____f.uuid}`)
    socket.on('connect', ()=>{
      
    })
    socket.on('reload', location.reload.bind(location))
  }

  function makeProxy(obj, paths){
    let f = obj.____f;
    let isNew = false
    if(paths){
      if(!f){
        isNew = true
        f = obj.____b || {
          id: model.____f.count++,
          paths
        }
        log.push({
          id: f.id,
          target: JSON.stringify(obj, (key, value) =>{
            if(typeof value !== 'object' || value === obj) return value
          }),
          f: f,
          action: 'new'
        })
      }
      else{
        f.paths = f.paths.concat(paths)
      }
    }
    if(!paths) delete obj.____f;
    if(!f && obj instanceof Array){
      f = JSON.parse(obj.shift())
    }
    if(typeof f === 'string') f = JSON.parse(f)
    Object.defineProperty(obj, '____f', {value: f})
    if(!hash[f.id]) hash[f.id] = obj
    else return obj
    for(let prop in obj){
      let value = obj[prop]
      if(typeof value === 'object' && value !== null){
        value = makeProxy(value, paths)
        obj[prop] = value
        if(isNew){
          log.push({
            id: obj.____f.id,
            prop,
            target: value.____f.id,
            action: 'set'
          })
        }
      }
    }
    return new Proxy(obj, {
      deleteProperty: (obj, prop)=>{
        addPendingTasks(CalcPath(obj.____f.paths, prop), model.____f.pendingTasks, model, undefined, obj)
        log.push({
          id: obj.____f.id,
          prop,
          action: 'delete'
        })
        delete obj[prop]
        return true
      },
      set: (obj, prop, value)=>{
        if(obj[prop] === value) return true
        let newPaths = CalcPath(obj.____f.paths, prop)
        if(typeof value === 'object' && value !== null){
          if(value.____f){
            obj[prop] = value
          }
          else{
            obj[prop] = makeProxy(value, newPaths)
          }
          log.push({
            id: obj.____f.id,
            prop,
            target: value.____f.id,
            action: 'set'
          })
        } 
        else{
          obj[prop] = value
          log.push({
            id: obj.____f.id,
            prop,
            value,
            action: 'set'
          })
        }
        addPendingTasks(newPaths, model.____f.pendingTasks, model, obj[prop], obj)
        return true
      }
    })
  }

  function CalcPath(paths, prop){
    if(paths.length === 0) return ['*', prop]
    let newPaths = []
    paths.forEach(p => {
      newPaths.push(`${p}.*`)
      newPaths.push(`${p}.${prop}`)
    })
    return newPaths
  }

  function addPendingTasks(paths, pendingTasks, root, value){
    if(!root.watch) return
    paths.forEach(p => {
      if(typeof root.watch[p] === 'function'){
        pendingTasks.push(root.watch[p].bind(root, [value, obj, p]))
      }
    })
  }
  

  for(let m in methods.frontend) model[m] = methods.frontend[m]
  methods.backend.forEach((m) => {
    model[m] = function(...args){
      args = args.map(arg => {
        if(typeof arg === 'object'){
          if(!arg.____f) arg = makeProxy(arg, ['?'])
          return {id: arg.____f.id}
        }
        return arg
      })
      socket.emit(m, log, args, (data) => {
        model.____f.count = Date.now()
        data.forEach(d => {
          if(d.action === 'new'){
            d.____b.id = d.id
            hash[d.id] = makeProxy(JSON.parse(d.target), d.____b.paths)
          }
          if(d.action === 'set'){
            if(d.target){
              d.value = hash[d.target]
            }
            hash[d.id][d.prop] = d.value
          }
          if(d.action === 'delete'){
            delete hash[d.id][d.prop] 
          }
        })
        if(data.length){
          doRender(template, [])
        }
      })
      log = []
    }
  })

  function doRender(template, params){
    if(typeof model['beforeRender'] === 'function'){
      model['beforeRender']()
    }
    render(template, params)
    if(typeof model['afterRender'] === 'function'){
      model['afterRender']()
    }
  }

  model = makeProxy(model)
  window.el = document.getElementById(model.____f.uuid)
  window.template = template
  window.model = model
  window.methods = methods
  window.hash = hash
  window.log = log

  assignDOM(el, template, [], [])

  if(methods.backend.length){
    let script = document.createElement('script')
    script.src = '/socket.io/socket.io.js'
    script.onload = socketIO
    head.appendChild(script)
  }

  if(typeof model['mounted'] === 'function'){
    let temp1 = log.length
    model['mounted']()
    if(temp1 !== log.length) doRender(template, [])
  }
  console.timeEnd('Backfront loading')
}