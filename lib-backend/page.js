import { Parse } from './parse'
import { Render } from './render'
import { RenderJs } from './render-js'
import { Sass } from './build-sass'
import { Register } from './socket'
import { MakeProxy } from './proxy'
import fs from 'fs'
import path from 'path'

let placeholder = '____PLACEHOLDER____'

export async function PreparePage(Page, config){
  let obj = new Page()

  let frontendMethods = ['mounted', 'afterRender', 'beforeRender']
  let methods = Object.getOwnPropertyNames(Object.getPrototypeOf(obj))
  let functions = []

  let methodsJson = JSON.stringify({
    backend: methods.filter(m => obj[m].isBackend), 
    frontend: methods.reduce((o, m)=>{
      if(obj[m].isFrontend || ~frontendMethods.indexOf(m)){
        o[m] = obj[m]
      }
      return o
    }, {})
  }, (key, value) => {
    if(typeof value === 'function'){
      functions.push(value.toString().split('\n').map(c => c.trim()).join(''))
      return placeholder
    }
    return value
  }).replace(new RegExp(`"${placeholder}"`, 'g'), () => functions.shift())

  try{
    let file = path.join(process.cwd(), config.templates, obj.template)
    if(file.split('.').length === 1) file += '.html'
    obj.template = fs.readFileSync(file).toString()
  } catch(e){}
  let template = Parse(obj.template)
  let templateJson = JSON.stringify(template)

  let layout;
  if(obj.layout){
    let file = path.join(process.cwd(), config.templates, 'layouts', obj.layout)
    if(file.split('.').length === 1) file += '.html'
    obj.layout = fs.readFileSync(file).toString()
    obj.layout = obj.layout.replace('{{body}}', '___BODY___')
    layout = Parse(obj.layout)
  }

  let css = await Sass(obj.style, config)

  config.app.get(obj.route, (req, res) => {
    console.time(`Render ${Page.name}`)
    let page = new Page()
    page.isBackend = true
    delete page.template
    page = MakeProxy(page)

    let response = ()=>{
      if(typeof page.beforeRender === 'function'){
        page.beforeRender()
      }
      let html = Render(template, page)
      if(typeof page.afterRender === 'function'){
        page.afterRender()
      }
      delete page.isBackend;
      page.isFrontend = true;

      let js = RenderJs(templateJson, page, methodsJson)
      html = `<style>${css}</style>${html}<script>${js}</script>`
      if(layout){
        html = Render(layout, page, true).replace('___BODY___', html)
      }
      res.end(html)
      Register(page, methods.filter(m => page[m].isBackend))
      console.timeEnd(`Render ${Page.name}`)
    }

    if(page.init){
      let result = page.init({
        params: req.params,
        query: req.query
      })
      if(result instanceof Promise) result.then(response, response)
      else response()
    }
    else response()
  })
}