import fs from 'fs'
import path from 'path'
import { Server } from 'http'
import Socket from 'socket.io'

import { PreparePage } from './page'
import { Connection } from './socket'

export function Init(options){
  console.log('Webpage App Loading...')
  console.time('Webpage App Started')
  options.pages.forEach(Page => PreparePage(Page, options))

  options.server = Server(options.app)
  options.io = Socket(options.server)

  options.server.listen(options.port, ()=>{
    console.timeEnd('Webpage App Started')
  })

  options.io.on('connection', Connection)
}