import uuidv4 from 'uuid/v4'

export function MakeProxy(obj, hash, paths, pendingTasks, root){
  let first = false
  if(!hash){
    first = true
    pendingTasks = []
    paths = []
    hash = {}
    
  }
  Object.defineProperty(obj, '____b', {
    value: obj.____f || {
      id: root ? root.____b.count++ : 0,
      paths: paths 
    }
  })
  delete obj.____f
  if(first){
    root = obj
    obj.____b.hash = hash 
    obj.____b.hashProxy = {}
    obj.____b.pendingTasks = pendingTasks
    obj.____b.count = 1
    obj.____b.newArrays = {}
    obj.____b.uuid = uuidv4()
    obj.____b.log = []
  }
  for(let prop in obj){
    if(prop === '____b') continue
    let value = obj[prop]
    if(typeof value === 'object' && value !== null){
      if(value.____b === undefined){
        value = MakeProxy(value, hash, CalcPath(paths, prop), pendingTasks, root)
      }
      obj[prop] = value
    }
  }
  hash[obj.____b.id] = obj
  let sets = []
  root.____b.log.push({
    id: obj.____b.id,
    target: JSON.stringify(obj, (key, value) =>{
      if(typeof value !== 'object' || value === obj) return value
      sets.push({
        id: obj.____b.id,
        prop: key,
        target: value.____b.id,
        action: 'set'
      })
    }),
    ____b: {
      paths: paths
    },
    action: 'new'
  })
  root.____b.log.push.apply(root.____b.log, sets)
  root.____b.hashProxy[obj.____b.id] = new Proxy(obj, {
    deleteProperty: (obj, prop)=>{
      addPendingTasks(CalcPath(paths, prop), pendingTasks, root, undefined, obj)
      delete obj[prop]
      root.____b.log.push({
        id: obj.____b.id,
        prop,
        action: 'delete'
      })
      return true
    },
    set: (obj, prop, value)=>{
      if(obj[prop] === value) return true
      let newPaths = CalcPath(paths, prop)
      if(typeof value === 'object' && value !== null){
        if(!value.____b){
          obj[prop] = MakeProxy(value, hash, newPaths, pendingTasks, root)
        }
        else{
          newPaths = newPaths.concat(obj.____b.paths)
        }
        root.____b.log.push({
          id: obj.____b.id,
          prop,
          target: value.____b.id,
          action: 'set'
        })
      } 
      else{
        obj[prop] = value
        root.____b.log.push({
          id: obj.____b.id,
          prop,
          value,
          action: 'set'
        })
      }
      addPendingTasks(newPaths, pendingTasks, root, obj[prop], obj)
      return true
    }
  })

  return root.____b.hashProxy[obj.____b.id]
}

function CalcPath(paths, prop){
  if(paths.length === 0) return ['*', prop]
  let newPaths = []
  paths.forEach(p => {
    newPaths.push(`${p}.*`)
    newPaths.push(`${p}.${prop}`)
  })
  return newPaths
}

function addPendingTasks(paths, pendingTasks, root, value){
  if(!root.watch) return
  paths.forEach(p => {
    if(typeof root.watch[p] === 'function'){
      pendingTasks.push(root.watch[p].bind(root, [value, obj, p]))
    }
  })
}