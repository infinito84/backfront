export function TextTemplate(str, params){
  let matches = str.match(/\{\{(.*?)\}\}/g)
  if(!matches){
    //str = str.replace(/\n/g, ' ')
    return str ? {text: str} : ''
  }
  let template = []
  matches.forEach((match) => {
    match = match.match(/\{\{(.*?)\}\}/)
    let text = str.split(match[0])
    if(text[0]) template.push(`'${text[0].replace(/\n/g, '\\n')}'`)
    str = text[1]
    template.push(match[1])
  })

  if(str) template.push(`'${str.replace(/\n/g, '\\n')}'`)
  str = template.join('+');
  return {
    params: params.slice(0),
    template: `return ${str}`,
    fn: Function.apply(null, params.concat(`return ${str}`))
  }
}