import HTMLParser from 'fast-html-parser'
import {TextTemplate} from './text-template'

let forIndex = 0
let input = ['input', 'textarea', 'select']
let args

function getForData(code){
  let tokens = code.split('in')
  let it = '$item'
  let index = '$index'
  if(tokens.length === 1){
    code = tokens[0]
  }
  else{
    it = tokens[0].trim().replace(/var|let|const|[\(\)]/g, '').split(',')
    code = tokens[1]
    if(it.length === 1) it = it[0]
    else{
      index = it[1]
      it = it[0]
    }
  } 
  return {it, index, code}
}

function getObject({rawText, tagName, attributes, childNodes, rawAttrs}, params){
  if(tagName){
    let obj = {
      tag: tagName
    }
    let attributes = rawAttrs.match(/(.*?)=(.*?)____(.*?)____/g)
    let block = false;
    if(attributes){
      attributes = attributes.reduce((o, item) => {
        let tokens = item.split('=')
        let key = tokens[0].trim()
        let value = args[tokens[1].trim()]
        if(~['"', "'"].indexOf(value[0])) value = value.substr(1, value.length - 2)
        if(~['@', ':'].indexOf(key[0])){
          key = key.substr(1)
          if(key === 'bind') obj.bind = value;
          if(key !== 'for' && key !== 'with'){
            value = `{{${value}}}`
          }
        }
        o[key] = value
        return o
      }, {})
      obj.attrs = attributes
      if(attributes.with){
        obj.with = true
        let withData = attributes.with.split('=').map(String.trim)
        let withParam = withData.shift()
        withData = withData.join('=')
        obj.attrs.with = TextTemplate(`{{${withData}}}`, params)
        params.push(withParam)
        obj.attrs.with.var = withParam
      }
      if(attributes.for){
        obj.for = true
        let forData = getForData(attributes.for)
        obj.attrs.for = TextTemplate(`{{${forData.code}}}`, params)
        params.push(forData.it, forData.index)
        obj.attrs.for.it = forData.it
        obj.attrs.for.index = forData.index
        obj.attrs.for.n = forIndex++
      }
      for(let attr in obj.attrs){
        if(attr === 'for') continue
        if(attr === 'with') continue
        if(attr === 'value' && attributes.bind) continue
        if(attr === 'bind'){
          block = true;
          if(~input.indexOf(tagName)){
            obj.children = []
            obj.attrs.value = TextTemplate(obj.attrs.bind, params)
            block = tagName !== 'select'
          }
          else{
            obj.children = [TextTemplate(obj.attrs.bind, params)]
          }
        }
        else{
          obj.attrs[attr] = TextTemplate(obj.attrs[attr], params)
        }
      }
      delete obj.attrs.bind
    }
    if(!block) obj.children = childNodes.map(c => getObject(c, params.slice(0))).filter(t => !!t)
    return obj
  }
  if(childNodes) return childNodes.map(c => getObject(c, params.slice(0))).filter(t => !!t)
  return TextTemplate(rawText.replace(/____(.*?)____/g, key => args[key]), params)
}

export function Parse(template){
  args = {}
  let count = 0
  template = template.replace(/"(.*?)"|'(.*?)'/g, function(match){
     let key = `____${count++}____`
     args[key] = match
     return key
  })
  forIndex = 0
  let html = HTMLParser.parse(template, {
    lowerCaseTagName: true,
    script: true,
    style: true,
    pre: true
  })
  return getObject(html, [])
}