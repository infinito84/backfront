import { MakeProxy } from './proxy'

let emptyTags = ['br', 'input', 'img', 'hr', 'meta', 'link']

function getAttributes(attrs, context, params){
  if(!attrs) return ''
  let filter = ['if', 'for', 'click']
  let attributes = []
  for(let attr in attrs){
    if(~filter.indexOf(attr)) continue
    let value = attrs[attr]
    if(value.fn){
      value = value.fn.apply(context, params).toString().replace(/"/g, '\\"')
    }
    else{
      value = value.text
    }
    attributes.push(`${attr}="${value}"`)
  }
  if(!attributes.length) return ''
  return ` ${attributes.join(' ')}`
}

function getHtml(e, context, params){
  if(e.with){
    params.push(e.attrs.with.fn.apply(context, params))
  }
  
  if(e.text) return e.text
  if(e.template){
    return e.fn.apply(context, params)
  }

  let render = (params)=>{
    if(e.attrs && e.attrs.if){
      let yes = !!e.attrs.if.fn.apply(context, params)
      if(e.tag === 'virtual' && !yes) return e.for ? '' : `<!--0--><!--0-->`
      if(!yes) return e.for ? '' : `<!--${e.tag}-->`
    }
    if(e.tag === 'virtual') return `<!--1-->${e.children.map(i => getHtml(i, context, params)).join('')}<!--1-->`
    if(~emptyTags.indexOf(e.tag)) return `<${e.tag}${getAttributes(e.attrs, context, params)}/>`
    return `<${e.tag}${getAttributes(e.attrs, context, params)}>${e.children.map(i => getHtml(i, context, params)).join('')}</${e.tag}>`
  }

  if(e.for){
    let array = e.attrs.for.fn.apply(context, params) || []
    if(!array.____b){
      // REVIEW: probably fails
      array = MakeProxy(array, context.____b.hash, [], context.____b.pendingTasks, context)
      context.____b.newArrays[array.____b.id] = array
    }
    let html = array.map((it, i) => render(params.concat(it, i))).filter(e => !!e)
    return `<!--${array.____b.id}-->${html.join('')}`
  }
  return render(params)
}

export function Render(template, context, layout){
  let html = ''
  if(template instanceof Array){
    html = template.map(e => getHtml(e, context, [])).join('')
  }
  else{
    html = getHtml(template, context, [])
  }
  if(layout) return html
  return `<div id="${context.____b.uuid}">${html}</div>`
}