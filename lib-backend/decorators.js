export function Backend(target, key, descriptor){
  descriptor.value.isBackend = true;
  return descriptor;
}

export function Frontend(target, key, descriptor){
  descriptor.value.isFrontend = true;
  return descriptor;
}