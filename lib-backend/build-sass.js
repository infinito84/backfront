import sass from 'node-sass'
import fs from 'fs'
import path from 'path'

let paths = []

function getFile(filename, rootPath){
  let file;
  let found = false;
  for(let i in paths){
    let dir = rootPath || paths[i]
    file = path.join(process.cwd(), dir, filename)
    if(['css', 'scss', 'sass'].indexOf(file.split('.').pop()) === -1) file += '.scss'
    if(fs.existsSync(file)){
      found = true
      break
    }
    file = file.replace('.scss', '.sass')
    if(fs.existsSync(file)){
      found = true
      break
    }
    let name = path.basename(file)
    file = file.replace(name, `_${name}`)
    if(fs.existsSync(file)){
      found = true
      break
    }
    file = file.replace('.sass', '.scss')
    if(fs.existsSync(file)){
      found = true
      break
    }
  }
  return found ? fs.readFileSync(file).toString() : ''
}

export async function Sass (style, config){
  paths = [config.styles]
  if(style){
    let data = getFile(style)
    return new Promise((resolve) => {
      sass.render({
        data,
        importer: (url, prev, done) => {
          let dir
          if(~url.indexOf('~')){
            dir = path.dirname(url)
            dir = dir.replace('~', '')
            dir = path.join('node_modules', dir)
            paths.push(dir)
            url = path.basename(url)
          }
          done({contents: getFile(url, dir)})
        }
      }, (err, result) => {
        if(err){
          console.error(err)
          resolve('')
        }
        else{
          resolve(result.css.toString().replace(/\n/g, ''))
        }
      })
    })
  }
  return Promise.resolve('')
}