import { MakeProxy } from './proxy'
let pages = {}

export function Connection(socket){
  let id = socket.handshake.query.id
  let data = pages[id]
  if(!data) return socket.emit('reload')
  registerPage(data, socket)
}

export function Register(page, methods){
  pages[page.____b.uuid] = {page, methods}
}

function createBackup(page){
  return Object.keys(page).reduce((obj, a) => {
    obj[a] = typeof page[a] === 'object' ? JSON.stringify(page[a]) : page[a]
    return obj
  }, {})
}

function updatePage(page, data){
  let hash = page.____b.hash
  data.forEach(d => {
    if(d.action === 'new'){
      hash[d.id] = JSON.parse(d.target)
      hash[d.id].____f = d.f
      MakeProxy(hash[d.id], page.____b.hash, d.f.paths, page.____b.pendingTasks, page)
    }
    if(d.action === 'set'){
      if(d.target){
        d.value = hash[d.target]
        if(d.value.____b === undefined){
          d.value = MakeProxy(d.value, page.____b.hash, d.value.____f.paths, page.____b.pendingTasks, page)
        }
      }
      hash[d.id][d.prop] = d.value
    }
    if(d.action === 'delete'){
      delete hash[d.id][d.prop] 
    }
  })
}

function registerPage({page, methods}, socket){
  methods.forEach((m) => {
    if(page[m].isBackend){
      socket.on(m, (data, args, callback)=>{
        updatePage(page, data)
        page.____b.count = Date.now()
        page.____b.log = []
        
        args = args.map(arg => {
          if(typeof arg === 'object'){
            return page.____b.hashProxy[arg.id]
          }
          return arg
        })

        let response = page[m](...args)
        if(response instanceof Promise){
          response.then(()=>{
            callback(page.____b.log)
            page.____b.log = []
          }, ()=>{
            callback(page.____b.log)
            page.____b.log = []
          })
        }
        else{
          callback(page.____b.log)
          page.____b.log = []
        }
      })
    }
  })
}