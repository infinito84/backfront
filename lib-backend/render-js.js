import fs from 'fs'
import path from 'path'

let script = fs.readFileSync(path.join(__dirname, 'frontend.js')).toString()

export function RenderJs(template, model, methods){
  let hash = model.____b.hash
  let hashProxy = model.____b.hashProxy
  delete model.____b.hash
  delete model.____b.hashProxy

  let idsRestore = []

  let modelJson = JSON.stringify(model, (key, value) => {
    if(value instanceof Array && value.____b){
      hash[value.____b.id].unshift(JSON.stringify(value.____b))
      idsRestore.push(value.____b.id)
    }
    else if(typeof value === 'object' && value.____b){
      hash[value.____b.id].____f = JSON.stringify(value.____b)
      idsRestore.push(value.____b.id)
    }
    return value
  })

  toRestore(hash, idsRestore)
  model.____b.hash = hash
  model.____b.hashProxy = hashProxy

  return `${script};window.backfront(${template}, ${modelJson}, ${methods})`
}

function toRestore(hash, ids){
  ids.forEach(id => {
    if(hash[id] instanceof Array) hash[id].shift()
    else delete hash[id].____f
  })
}