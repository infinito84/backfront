# Backfront

Isomorphic framework for easy Backend and Frontend development

## Getting Started

This framework is under development. See the [example code](https://glitch.com/edit/#!/backfront?path=pages/home.ts) and [live example](https://backfront.glitch.me/)

## Authors

* **Sergio Andrés Ñustes** - [infinito84](https://gitlab.com/infinito84)

## License

This project is licensed under the MIT License